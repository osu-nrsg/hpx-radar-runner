"""Get all recordings without a cube and try to process them. Some won't work because of
missing GPS data."""

import logging
from pathlib import Path

from nested_config import validate_config
from radar_postproc import KmzParams, make_and_send_kmz
from wimr_conversions import MovingPolarCubeParams, create_cube

handler = logging.StreamHandler()
handler.setLevel("DEBUG")
logger = logging.getLogger(__name__)
logger.setLevel("DEBUG")
logger.addHandler(handler)
logging.getLogger("wimr_conversions").setLevel("DEBUG")
logging.getLogger("wimr_conversions").addHandler(handler)


def get_uncubed_recordings(glob_pattern: str):
    recording_paths = Path("/data/recordings").glob(glob_pattern + ".nc")
    cube_paths = Path("/data/cubes").glob(glob_pattern + "_moving_pol.mat")
    cubed_recordings = [p.name[:24] + ".nc" for p in cube_paths]
    return sorted([p for p in recording_paths if p.name not in cubed_recordings])


def cube_and_kmz_recording(
    nc_path: Path, cube_params: MovingPolarCubeParams, kmz_params: KmzParams
):
    logger.info("Cubing %s", nc_path)
    try:
        cube = create_cube(nc_path, cube_params)
    except Exception as ex:
        logger.error("Could not cube %s: %s", nc_path, ex)
        return
    logger.info("Making KMZ for %s", nc_path)
    try:
        make_and_send_kmz(cube.cube_path, kmz_params)
    except Exception as ex:
        logger.exception("Could not create kmz from %s: %s", cube.cube_path, ex)


def main():
    uncubed_recordings = get_uncubed_recordings("2021-06-*/*")
    logger.info(f"{uncubed_recordings=}")
    cube_params = validate_config(
        "params/wimr-convert/PtSur_movingpol_params.toml", MovingPolarCubeParams
    )
    kmz_params = validate_config("params/postproc/PtSur_kmz_params.toml", KmzParams)
    for wimr_path in uncubed_recordings:
        cube_and_kmz_recording(wimr_path, cube_params, kmz_params)


main()
