#!/bin/bash
## In CRON, MUST BE RUN with /bin/bash radar_autoproc.bash because otherwise cron will run using csh
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $SCRIPTDIR/../
.venv/bin/hpx-radar-runner params/Newport_125s.toml --loglevel WARNING
