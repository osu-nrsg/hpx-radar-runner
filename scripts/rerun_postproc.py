#!/shared/Development/hpx-radar-runner/.venv/bin/python
"""rerun_postproc.py - Does any cubing and image processing that needs to be re-done for
Newport.

Looks for mssing image json files to determine if reprocessing needs to happen, so might
need a different approach for a different reprocessing scheme.
"""

import logging
from pathlib import Path

# isort: off
import matlab.engine  # noqa: F401 # MATLAB imported here to prevent errors on later import

# isort: on
from ncdataset import NCDataset
from nested_config import validate_config

from hpx_radar_runner.params import RadarRunnerParams
from hpx_radar_runner.radar_runner import cart_proc, pol_proc

logger = logging
logger.basicConfig(level="INFO")


def proc_90s(rec_path: Path):
    rr_params = validate_config(
        "/shared/Development/hpx-radar-runner/params/Newport_90s.toml", RadarRunnerParams
    )
    for pol_params in rr_params.pol_params:
        pol_proc(rec_path, pol_params)


def proc_13min(rec_path: Path):
    rr_params = validate_config(
        "/shared/Development/hpx-radar-runner/params/Newport_13min.toml",
        RadarRunnerParams,
    )
    for pol_params in rr_params.pol_params:
        pol_proc(rec_path, pol_params)
    for cart_params in rr_params.cart_params:
        cart_proc(rec_path, cart_params)


if __name__ == "__main__":
    for rec_path in Path("/data/recordings").glob("2022-*-*/*.nc"):
        json_name = f"{rec_path.stem[:-2]}.json"
        json_path = Path("/data/images/") / rec_path.parent.name / json_name
        if not json_path.is_file():
            with NCDataset(rec_path) as ds:
                duration = ds["time"][-1]
            try:
                logger.info("-- Processing %s", rec_path)
                if duration < 92:
                    proc_90s(rec_path)
                else:
                    proc_13min(rec_path)
            except Exception:
                logger.exception("Failed to process %s", rec_path)
        else:
            logger.info("-- Skipping %s", rec_path)

# Find recordings
# if json file is missing, run processing
