# README - OSU-NRSG Configurations  <!-- omit in toc -->

Python application for executing a radar recording, processing, and post-processing pipeline. Parameter files
are also stored in this repo.

This repository shall serve as a single point for storing configuration files for OSU-NRSG radar data
acquisition and processing.

Tags might be made to archive config used for certain experiments.
