%% User options for polMat2TimexKmz.m %%

radarLatitude      = [];% Leave blank [] to use UTM coordinates in Cube.results
radarLongitude     = [];% Leave blank [] to use UTM coordinates in Cube.results
heading            = [];% Leave blank [] to use heading in Cube.results
aziLims            = []; % Two-element vector of [Start End] azimuths to plot in KMZ (in degrees). If no cropping, keep empty or assign to [-inf inf]
rgLims             = []; % Two-element vector of [Start End] ranges to plot in KMZ (in meters). If no cropping, keep empty or assign to [-inf inf]
rangeDecimation    = 1;           % Decimate ranges by this factor to speed up interpolation to Cartesian
cartGrid_dx        = 5;          % kmz requires cartesian grid - this is spatial resolution
cartGrid_dy        = 5;          % kmz requires cartesian grid - this is spatial resolution
colorAxisLimits    = [];    % caxis (if empty, defaults to [0 255])
colorMap           = colorcet('fire'); % colormap to use (make sure it is in the Matlab path!
altitude           = 5; % meters above ground level
drawOrder          = 10;        % larger value means it plots on top in Google Earth
pngSharpness       = 5;         % "Crispness" factor in png generation. [1-5]. Larger is more pixelated.
local_tz           = 'America/Los_Angeles';
local_tz_abbr      = {'PST', 'PDT'};
ge_toolboxPath     = '/shared/matlab_toolboxes/googleearth';  % Path to GE Toolbox
