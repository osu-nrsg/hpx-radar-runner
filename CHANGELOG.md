# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2024-05-15

- TODO

## [0.2.6] - 2023-01-24

- Add Florence params and some new Newport params
- Updated dependencies
  - hpx-radar-recorder 1.3.0
  - wimr-conversions 0.8.0
  - radar-postproc 0.3.0
  - Minimum Python 3.9
- params - Add general validator for converting strings to Models
- Add new SpectralTimeseriesParams from radar-postproc
- Python 3.9 - don't need List, Dict, Tuple types anymore

## [0.2.5] - 2022-02-08

- Split out cubing and postprocessing
- Fix some type annotations
- Update wimr-conversions
- rerun_postproc.py script

## [0.2.4] - 2021-08-05

- Revert to pydantic-plus

## [0.2.3] - 2021-08-04

- Fix order of classmethod and validate_arguments decorators in BaseModel

## [0.2.2] - 2021-08-04

- Update some dependencies to new versions
- Remove pydantic-plus and use local module instead.

## [0.2.1] - 2021-07-06

- Add some shell scripts and params
- Update pyproject.toml with some fixes in wimr-conversions and radar-postproc

## [0.2.0] - 2021-06-16

- Adapted to updated wimr-conversions including moving polar params
- Added a bunch of params files

## [0.1.0] - 2021-05-13

- Initial release

[Unreleased]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.6...master
[0.2.6]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.5...v0.2.6
[0.2.5]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.4...v0.2.5
[0.2.4]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/osu-nrsg/hpx-radar-runner/-/tags/v0.1.0
