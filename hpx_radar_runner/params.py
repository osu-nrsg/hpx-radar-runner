from __future__ import annotations

from typing import Any, Optional

from hpx_radar_recorder import RadarParameters, RecordingParameters, SiteParameters
from pydantic import BaseModel
from radar_postproc import (
    ImageryGenParams,
    KmzParams,
    SpectraGenParams,
    SpectralTimeseriesParams,
)
from wimr_conversions import CartCubeParams, MovingPolarCubeParams, PolarCubeParams


class PolarParams(BaseModel):
    cube_params: PolarCubeParams
    co_rect_params: Optional[Any] = None  # Not yet implemented
    co_geo_rect_params: Optional[Any] = None  # Not yet implemented
    kmz_params: Optional[KmzParams] = None
    imagery_params: Optional[ImageryGenParams] = None


class MovingPolarParams(BaseModel):
    cube_params: MovingPolarCubeParams
    kmz_params: Optional[KmzParams] = None
    imagery_params: Optional[ImageryGenParams] = None


class CartParams(BaseModel):
    cube_params: CartCubeParams
    spectra_params: Optional[SpectraGenParams] = None
    timeseries_params: Optional[SpectralTimeseriesParams] = None


class RadarRunnerParams(BaseModel):
    recording_params: RecordingParameters
    radar_params: RadarParameters
    site_params: SiteParameters
    pol_params: list[PolarParams] = []
    cart_params: list[CartParams] = []
    moving_pol_params: list[MovingPolarParams] = []


# SpectraGenParams.update_forward_refs()
