import logging
import sys
import time
from enum import IntEnum
from pathlib import Path
from typing import Optional

# isort: off
import matlab.engine  # noqa: F401 # MATLAB imported here to prevent errors on later import

# isort: on
import hpx_radar_recorder
import radar_postproc
import typer
from hpx_radar_recorder.logging import set_up_logging
from nested_config import expand_config
from typing_extensions import Annotated
from wimr_conversions import CartCube, MovingPolarCube, PolarCube, create_cube

from .params import CartParams, MovingPolarParams, PolarParams, RadarRunnerParams

logger = logging.getLogger(__name__)
PACKAGE_NAME = __name__.split(".")[0]

_exit_code_docstrings = {
    0: "Recording finished successfully",
    1: "server stopped reporting status/config",
    2: "There is a validation error with the supplied parameters",
    3: "The supplied parameters cause an error configuring the recorder",
    4: (
        "The recording did not complete successfully (some sweeps may have been written"
        " to file)"
    ),
    255: "Other error",
}


class ExitCode(IntEnum):
    """Exit codes for cli"""

    OK = 0
    STATUS_TIMEOUT = 1
    PARAMS_ERROR = 2
    CONFIG_ERROR = 3
    RECORDING_ERROR = 4
    UNKNOWN_ERROR = 255

    @property
    def _doc(self):
        return _exit_code_docstrings[self.value]

    @classmethod
    def doc(cls) -> str:
        return "ExitCode values:\n" + "\n".join(
            f"{v.value:3d} {v.name:15s} {v._doc}" for v in cls
        )


def main(rr_params: RadarRunnerParams, nsweep: Optional[int] = None):
    class frame_i_logger:
        def __init__(self, nframes, loglevel=logging.DEBUG):
            self.nframes = nframes
            self._next_print = 0
            self._loglevel = loglevel

        def __call__(self, frame_i):
            pct_complete = 100 * (frame_i + 1 / self.nframes)
            if pct_complete > self._next_print:
                logger.log(
                    self._loglevel, "%3d of %3d frames written", frame_i + 1, self.nframes
                )
                self._next_print += 1

    class frame_t_logger:
        def __init__(self, total_s, loglevel=logging.DEBUG):
            self.total_s = total_s
            self.tstart = None
            self._next_print = 0
            self._loglevel = loglevel

        def __call__(self, frame_i):
            """Print elapsed time every 1%"""
            if self.tstart is None:
                self.tstart = time.time()
            elapsed = time.time() - self.tstart
            pct_complete = 100 * (elapsed / self.total_s)
            if pct_complete > self._next_print:
                logger.log(
                    self._loglevel,
                    "%.f s of %.f s elapsed, %d frames written",
                    elapsed,
                    self.total_s,
                    frame_i + 1,
                )
                self._next_print += 1

    if nsweep:
        rr_params.recording_params.num_sweeps = nsweep
    frame_logger = (
        frame_i_logger(rr_params.recording_params.num_sweeps)
        if rr_params.recording_params.num_sweeps > 0
        else frame_t_logger(rr_params.recording_params.duration.seconds)
    )
    recording = hpx_radar_recorder.Recording(
        rr_params.recording_params,
        rr_params.radar_params,
        rr_params.site_params,
        frame_written_callback=frame_logger,
    )
    logger.info("Starting recording")
    recording.run()
    logger.info("Recording complete.")
    nc_path = recording.nc_path

    # Pol cubing and processing
    try:
        for pol_params in rr_params.pol_params:
            pol_proc(nc_path, pol_params)

        if rr_params.moving_pol_params:
            # Delay to let sufficient gps data to collect
            time.sleep(5)
            for mov_pol_params in rr_params.moving_pol_params:
                moving_pol_proc(nc_path, mov_pol_params)

        # Cart cubing and processing
        for cart_params in rr_params.cart_params:
            cart_proc(nc_path, cart_params)

    except Exception as ex:
        err_text = f"There was a problem processing the recording {nc_path}: {ex}"
        logger.exception(err_text)
        logger.debug("Error Traceback:\n", exc_info=sys.exc_info())
        return 5, err_text


def pol_proc(nc_path: Path, pol_params: PolarParams):
    logger.info("Creating polar cube of %s", nc_path.name)
    cube: PolarCube = create_cube(nc_path, pol_params.cube_params)
    logger.info("Polar cubing complete")
    pol_postproc(cube.cube_path, pol_params)


def pol_postproc(pol_cube_path: Path, pol_params: PolarParams):
    if pol_params.co_rect_params or pol_params.co_geo_rect_params:
        raise NotImplementedError("Haven't implemented coRect or coGeoRect yet.")
    if pol_params.kmz_params:
        logger.info("Creating KMZ from %s", pol_cube_path.name)
        radar_postproc.make_and_send_kmz(pol_cube_path, pol_params.kmz_params)
        logger.info("KMZ finished")
    if pol_params.imagery_params:
        logger.info("Creating imagery from %s", pol_cube_path.name)
        imagery_paths, json_path = radar_postproc.plot_and_send_imagery(
            pol_cube_path, pol_params.imagery_params
        )
        logger.info("Imagery files created: %s", imagery_paths)
        logger.info("JSON file created: %s", json_path)
        logger.info("Imagery finished.")


def moving_pol_proc(nc_path: Path, mov_pol_params: MovingPolarParams):
    logger.info("Creating moving polar cube of %s", nc_path.name)
    cube: MovingPolarCube = create_cube(nc_path, mov_pol_params.cube_params)
    logger.info("Moving Polar cubing complete")
    if mov_pol_params.kmz_params:
        # FUTURE: Log KMZ filename
        logger.info("Creating KMZ from %s", cube.cube_path.name)
        radar_postproc.make_and_send_kmz(cube.cube_path, mov_pol_params.kmz_params)
        logger.info("KMZ finished.")
    if mov_pol_params.imagery_params:
        logger.info("Creating imagery from %s", cube.cube_path.name)
        imagery_paths, json_path = radar_postproc.plot_and_send_imagery(
            cube.cube_path, mov_pol_params.imagery_params
        )
        logger.info("Imagery files created: %s", imagery_paths)
        logger.info("JSON file created: %s", json_path)
        logger.info("Imagery finished.")


def cart_proc(nc_path: Path, cart_params: CartParams):
    logger.info("Creating Cartesian cube of %s", nc_path.name)
    cube: CartCube = create_cube(nc_path, cart_params.cube_params)
    cart_postproc(cube.cube_path, cart_params)


def cart_postproc(cart_cube_path: Path, cart_params: CartParams):
    if cart_params.spectra_params:
        logger.info("Creating spectral plot from %s", cart_cube_path.name)
        radar_postproc.plot_and_send_spectra(cart_cube_path, cart_params.spectra_params)
        logger.info("Spectra plotting complete")
    if cart_params.timeseries_params:
        logger.info("Creating timeseries plots")
        radar_postproc.plot_and_send_spectral_timeseries(cart_params.timeseries_params)
        logger.info("Timeseries plotting complete")


app = typer.Typer(rich_markup_mode="markdown")


@app.command()
def cli(
    params_path: Annotated[
        Path, typer.Argument(help="Path to the radar-runner parameters file")
    ],
    nsweep: Annotated[
        Optional[int],
        typer.Option(
            help=(
                "Number of sweeps to record, overriding the value in the"
                " RecordingParameters file"
            )
        ),
    ] = None,
    logfile: Annotated[
        Optional[Path],
        typer.Option(
            help="Path for the log file. If none is provided, logging will be to stderr"
        ),
    ] = None,
    loglevel: Annotated[
        str,
        typer.Option(
            help="Level to log in logfile [DEBUG, INFO, WARNING, ERROR, CRITICAL]"
        ),
    ] = "INFO",
):
    """Make a recording using hpx-radar-recorder from an hpx-radar-server instance and
    then perform various levels of processing (wimr_conversion) and postprocessing
    (radar_postproc).

    Note: In order to do some of the postprocessing it is necessary to run
    `install_mat_eng`, and then `hpx-radar-runner` must be run as a user licensed to use
    MATLAB on this machine.
    """
    for pkg_name in [
        PACKAGE_NAME,
        "hpx_radar_recorder",
        "radar_postproc",
        "wimr_conversions",
    ]:
        set_up_logging(pkg_name, logfile, loglevel)
    try:
        params = RadarRunnerParams.model_validate(
            expand_config(params_path, RadarRunnerParams)
        )
    except Exception:
        logger.exception(
            "The configuration at %s could not be processed into a RadarRunnerParams"
            " instance.",
            params_path,
        )
        return

    try:
        main(params, nsweep)
    except Exception:
        logger.exception("%s failed to complete execution.", __name__)
        logger.debug("Parameters: %s", params)


if __name__ == "__main__":
    app()
